# My most often repeated review feedback

Hi all!

This repository contains examples used for my talk "My most often repeated review feedback" held at ZGPHP Meetup held on
January 18th 2024.

The repository contains two examples of features as they might be implemented by a junior developer on my team. I then commented on the merge request what could be improved and created another MR which implements the recommendation.

The first example has two MRs:

1. [prevent clients originating from some countries from making a request](https://gitlab.com/luka.papez/zgphp-meetup-demo/-/merge_requests/1)
2. [but then refactor the code to make it easier to test](https://gitlab.com/luka.papez/zgphp-meetup-demo/-/merge_requests/2)

The second example also has two MRs:

3. [implement report generation](https://gitlab.com/luka.papez/zgphp-meetup-demo/-/merge_requests/3)
4. [but also make it more robust and easier to extend](https://gitlab.com/luka.papez/zgphp-meetup-demo/-/merge_requests/4)

If you want to discuss these topics further, feel free to reach out to me directly via [email](mailto:luka.papez@outlook.com) or [LinkedIn](https://www.linkedin.com/in/luka-papez/) or via anyone at [CactusCode](https://cactus-code.com/).
